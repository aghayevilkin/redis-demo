package com.example.Redis.Demo.api;

import com.example.Redis.Demo.entity.User;
import com.example.Redis.Demo.service.TestService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/test")
@RequiredArgsConstructor
public class RedisCacheContorller {

    private final TestService testService;

    @GetMapping
    public String getPerson() throws InterruptedException {
        return testService.get();
    }
    @PostMapping("/create")
    public void create(){
        testService.create();
    }
    @GetMapping("/{userId}")
    public Optional<User> getUser(@PathVariable String userId){
        return testService.getUser(userId);
    }
}
