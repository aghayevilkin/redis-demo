package com.example.Redis.Demo.service;

import com.example.Redis.Demo.entity.User;
import com.example.Redis.Demo.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TestService {

    @Autowired
    private UserRepository repository;

    @Cacheable(cacheNames = "testCache")
    public String get() throws InterruptedException {
        Thread.sleep(5000L);
        return "this is a test message!";
    }

    public void create() {
        User user = new User();
        user.setName("test");
        user.setAge(5);
        repository.save(user);
    }

    public Optional<User> getUser(String userId) {
        return repository.findById(userId);
    }
}
